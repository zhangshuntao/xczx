window.addEventListener('load', function () {
    var person = document.getElementById('person');
    var personA = document.getElementById('personA');
    var personH = document.getElementById('personH');

    var banner = document.querySelector('.banner');
    var img = banner.querySelector('img');
    var ol = banner.querySelector('ol');

    var navList = document.querySelector('.navList');
    var navLis = navList.querySelectorAll('li');
    var main = document.querySelectorAll('.main');
    var conSon = document.querySelectorAll('.main>div');

    var top = document.querySelector('.top');
    var span = top.querySelector('span');

    var dataCon = document.querySelector('.dataCon');
    var ConlD = dataCon.querySelector('.ConlD ');
    var prograCon = document.querySelector('.prograCon');
    var ConlP = prograCon.querySelector('.ConlP ');

    var ConrPD = document.querySelector('.ConrPD');
    var rtit = ConrPD.querySelector('.rtit');
    var rcon = ConrPD.querySelector('.rcon');

    timer();
    // 首页获取并显示数据start
    personA.addEventListener('mouseover', function () {
        personH.style.display = 'block';
    });
    var dataU = localStorage.getItem('username');
    var datap = localStorage.getItem('password');
    if (dataU != '') {
        personA.innerHTML = '欢迎 ' + dataU + '！';
    }
    // 首页获取并显示数据end
    // 轮播图制作start
    var num = 0;
    var circle = num;
    var a = 'images/index/banner2.png';
    var b = 'images/index/banner11 (1).jpeg';
    var c = 'images/index/banner11 (2).jpeg';
    var d = 'images/index/banner11 (3).jpeg';
    var e = 'images/index/banner11 (4).jpeg';
    var f = 'images/index/banner11 (5).jpeg';
    var arr = [a, b, c, d, e, f];
    for (var i = 0; i < arr.length; i++) {
        var li = document.createElement('li');
        ol.appendChild(li);
        ol.children[0].className = 'cur';
    }
    var time;
    function timer() {
        time = setInterval(function () {
            num++;
            if (num >= arr.length) {
                num = 0;
            }
            img.src = arr[num];
            for (var i = 0; i < ol.children.length; i++) {
                ol.children[i].className = '';
            }
            ol.children[num].className = 'cur';
        }, 1000);
    }
    banner.addEventListener('mouseover', function () {
        clearInterval(time);
    })
    banner.addEventListener('mouseout', function () {
        timer();
    })
    // 轮播图制作end
    // 测导航栏模块start
    for (var i = 0; i < navLis.length; i++) {
        navLis[i].setAttribute('index', i);
        navLis[i].addEventListener('mouseover', function () {
            index = this.getAttribute('index');
            var distance = conSon[index].offsetTop;
            window.scroll(0, distance);
        })
    }
    document.addEventListener('scroll', function () {
        for (var j = 0; j < conSon.length; j++) {
            if (window.pageYOffset > (conSon[j].offsetTop)
            ) {
                for (var a = 0; a < conSon.length; a++) {
                    navLis[a].style.color = '';
                }
                navLis[j].style.color = '#37b7ff';
            }
        }
        if (window.pageYOffset >= conSon[1].offsetTop) {
            top.style.display = 'block';
            ConlP.style.display = 'block';
        }
        if (window.pageYOffset >= conSon[2].offsetTop) {
            ConlD.style.display = 'block';
        }
    })
    // 测导航栏模块end
    // 返回顶部按钮start
    top.addEventListener('click', function () {
        window.scroll(0, 0);
    })
    ConrPD.addEventListener('mouseover', function () {
        ConrPD.style.position = 'relative';
        rtit.style.position = 'absolute';
        rtit.style.bottom = 0;
        rcon.style.position = 'absolute';
        rcon.style.top = 0;
        rcon.style.marginTop = 0;
    })
    ConrPD.addEventListener('mouseout', function () {
        ConrPD.style.position = '';
        rtit.style.position = '';
        rcon.style.position = '';
        rcon.style.marginTop = 0;
    })

    // 返回顶部按钮end
})