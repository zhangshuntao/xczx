window.addEventListener('load', function () {
    var tel = document.getElementById('tel');
    var mes = document.getElementById('mes');
    var psw = document.getElementById('ps');
    var pswSur = document.getElementById('pswSur');
    var chec = document.getElementById('chec');
    var btn = document.getElementById('btn');
    var telReg = new RegExp('^1[3456789]\\d{9}$');
    var pswReg = new RegExp('^\\w{6,16}$');
    btn.addEventListener('click', function () {
        if (tel.value == '') {
            alert('请输入11位手机号码');
        } else if (!telReg.test(tel.value)) {
            alert('手机号错误');
        } else if (mes.value == '') {
            alert('请输入6位短信验证码');
        } else if (psw.value == '') {
            alert('请输入密码');
        } else if (!pswReg.test(psw.value)) {
            alert('请输入6~16位密码');
        } else if (pswSur.value == '') {
            alert('再次输入密码');
        } else if (!pswReg.test(pswSur.value)) {
            alert('再次输入6~16位密码');
        } else if (psw.value != pswSur.value) {
            alert('两次密码输入不一致');
        } else if (!chec.checked) {
            alert('请勾选同意协议并注册');
        } else {
            window.location.href = 'login.html';
        }
    })
})