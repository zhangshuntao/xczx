$(function () {
    // 顶部个人中心
    var user = $("#user");
    var dataU = localStorage.getItem('username');
    // console.log(dataU);
    if (dataU != '') {
        // user.innerHTML = '欢迎 ' + dataU + '！';
        user.html("欢迎 " + dataU + "！");
    } else {
        user.html('个人中心');
    }

    //顶部输入框焦点事件
    var input = $(".search input");
    input.focus(function () {
        // console.log(this.placeholder);
        if (this.placeholder === '输入关键字') {
            this.placeholder = '';
        }
    });
    input.blur(function () {
        if (this.placeholder === '') {
            this.placeholder = '输入关键字';
        }
    });

    // mian主题部分recommend-hd切换页面
    // 获取元素
    var as = $(".recommend-hd ul li ");
    // console.log(as);
    // 给元素绑定事件
    as.click(function () {
        // 1.点击上部的li，当前li 添加current类，其余兄弟移除类
        $(this).addClass("current").siblings().removeClass("current");
        // 2.点击的同时，得到当前li 的索引号
        var index = $(this).index();
        // 3.让下部里面相应索引号的item显示，其余的item隐藏
        $(".recommend-bd .nav").eq(index).show().siblings().hide();
    });


    // 右侧分页功能
    var left = $(".left");
    var right = $(".right");
    var l = 2;
    var r = 0;
    left.click(function () {
        var n = $(this).siblings(".itxt").val();
        if (n == 1) {
            return false;
        }
        n--;
        $(this).siblings(".itxt").val(n);
        l--;
        $(this).addClass("blue").siblings().removeClass("blue");
        $(".recommend-bd .nav").eq(l).show().siblings().hide();
    })
    right.click(function () {
        // 得到当前兄弟文本框的值
        var n = $(this).siblings(".itxt").val();
        if (n == 3) {
            return false;
        }
        n++;
        $(this).siblings(".itxt").val(n);
        r++;
        $(this).addClass("blue").siblings().removeClass("blue");
        $(".recommend-bd .nav").eq(r).show().siblings().hide();
    })

    // main主体底部分页器
    var prev = $(".prev");
    var next = $(".next");
    var btn = $(".btn");
    var first = $(".first");
    var last = $(".last");
    var num = -1;
    var newPage = 0
    // console.log(pages);
    // 给元素绑定事件
    first.click(function () {
        $(this).addClass("blue").siblings().removeClass("blue");
        $(".recommend-bd .nav").eq(0).show().siblings().hide();
    })
    last.click(function () {
        $(this).addClass("blue").siblings().removeClass("blue");
        $(".recommend-bd .nav").eq(btn.length - 5).show().siblings().hide();
    })
    prev.click(function () {
        newPage--;
        if (newPage == -1) {
            return false
        }
        $(this).addClass("blue").siblings().removeClass("blue");
        $(".recommend-bd .nav").eq(newPage).show().siblings().hide();
    })
    next.click(function () {
        newPage++;
        $(this).addClass("blue").siblings().removeClass("blue");
        $(".recommend-bd .nav").eq(newPage).show().siblings().hide();
    })
    btn.click(function () {
        num++;
        $(this).addClass("blue").siblings().removeClass("blue");
        $(".recommend-bd .nav").eq(num).show().siblings().hide();
        newPage = num;
    })

    // 侧边栏固定定位
    // (1) 获取元素
    var top = $(".top");
    var main = $(".main");
    // console.log(main[0].offsetTop);
    var mianTop = main[0].offsetTop;
    // console.log(top);
    // (2) 页面滚动事件
    $(document).scroll(function () {
        // console.log(window.pageYOffset);
        // (3) 当页面的头部被卷去的部分大雨207时将侧边栏改为固定定位
        if (window.pageYOffset >= mianTop) {
            top.css("position", "fixed")
            top.css("right", 160 + 'px')
        } else {
            top.css("position", "absolute")
            top.css("right", 0 + 'px')
        }
    })
})

