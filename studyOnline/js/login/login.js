window.addEventListener("load",function(){
    var user = document.getElementById('user');
    var psw = document.getElementById('psw');
    var btn = document.getElementById('btn');
    var form = document.getElementsByTagName('form');
    btn.addEventListener('click',function(){
        if(user.value == '' && psw.value == '') {
            alert('请输入用户名和密码');
        } else if(user.value == '') {
            alert('请输入用户名');
        } else if(psw.value == '') {
            alert('请输入密码');
        } else {
            window.location.href='index.html';
            localStorage.setItem('username',user.value);
            localStorage.setItem('password',psw.value);
        }
    })
})