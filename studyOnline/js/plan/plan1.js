$(function () {
    //获取图片内容对象和包含图片内容和滑动条对象的div对象
    var scrollPane = $(".scroll-pane"),
        scrollContent = $(".scroll-content");

    //获取滑动条对象并进行相应的设置
    var scrollbar = $(".scroll-bar").slider({
        //设置发生滑动滑动柄事件时的触发事件
        slide: function (event, ui) {
            if (scrollContent.width() > scrollPane.width()) {
                scrollContent.css("margin-left", Math.round(
                    ui.value / 100 * (scrollPane.width() - scrollContent.width())
                ) + "px");
            } else {
                scrollContent.css("margin-left", 0);
            }
        }
    });

    //改变图片的处理
    var handleHelper = scrollbar.find(".ui-slider-handle")
        .mousedown(function () {
            scrollbar.width(handleHelper.width());
        })
        .mouseup(function () {
            scrollbar.width("100%");
        })
        .append("<span class='ui-icon ui-icon-grip-dotted-vertical'></span>")
        .wrap("<div class='ui-handle-helper-parent'></div>").parent();

    //设置超出的图片处于隐藏状态
    scrollPane.css("overflow", "hidden");

    //设置滚动条滚动距离的大小和处理的比例
    function sizeScrollbar() {
        var remainder = scrollContent.width() - scrollPane.width();
        var proportion = remainder / scrollContent.width();
        var handleSize = scrollPane.width() - (proportion * scrollPane.width());
        scrollbar.find(".ui-slider-handle").css({
            width: handleSize,
            "margin-left": -handleSize / 2
        });
        handleHelper.width("").width(scrollbar.width() - handleSize);
    }

    //获取滚动内容图片位置而设置整滑动柄的值
    function resetValue() {
        var remainder = scrollPane.width() - scrollContent.width();
        var leftVal = scrollContent.css("margin-left") === "auto" ? 0 :
            parseInt(scrollContent.css("margin-left"));
        var percentage = Math.round(leftVal / remainder * 100);
        scrollbar.slider("value", percentage);
    }

    //根据窗口大小设置显示图片内容
    function reflowContent() {
        var showing = scrollContent.width() + parseInt(scrollContent.css("margin-left"), 10);
        var gap = scrollPane.width() - showing;
        if (gap > 0) {
            scrollContent.css("margin-left", parseInt(scrollContent.css("margin-left"), 10) + gap);
        }
    }

    //根据窗口大小调整滑动柄上的位置
    $(window).resize(function () {
        resetValue();
        sizeScrollbar();
        reflowContent();
    });

    setTimeout(sizeScrollbar, 10);//0.1秒后执行方法sizeScrollbar
});