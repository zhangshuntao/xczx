$(function () {
    var user = $("#user");
    var dataU = localStorage.getItem('username');
    console.log(dataU);
    if (dataU != '') {
        // user.innerHTML = '欢迎 ' + dataU + '！';
        user.html("欢迎 " + dataU + "！");
    } else {
        user.html('个人中心');
    }

    //1.    鼠标经过对应文本高亮
    $(".content_nav li").hover(function () {
        $(this).siblings().stop().fadeTo(400, 0.5);
        $(this).addClass("li_blue").siblings().removeClass('li_blue')
    }, function () {
        //鼠标离开
        $(this).siblings().stop().fadeTo(400, 1);
    });
    $(".content_nav li").click(function () {
        var index = $(this).index();
        $(".box_tab .item").eq(index).show().siblings().hide()
    })
    $(".text-0").click(function () {
        let a = $(".pull_text-0");
        if (a.css("display") == "none") {
            a.show();
        } else {
            a.hide();
        }
    });
    $(".text-1").click(function () {
        let a = $(".pull_text-1");
        if (a.css("display") == "none") {
            a.show();
        } else {
            a.hide();
        }
    });
    $(".text-2").click(function () {
        let a = $(".pull_text-2");
        if (a.css("display") == "none") {
            a.show();
        } else {
            a.hide();
        }
    }); $(".text-3").click(function () {
        let a = $(".pull_text-3");
        if (a.css("display") == "none") {
            a.show();
        } else {
            a.hide();
        }
    });
    $(".text-4").click(function () {
        let a = $(".pull_text-4");
        if (a.css("display") == "none") {
            a.show();
        } else {
            a.hide();
        }
    });

    // 侧边栏固定定位
    // (1) 获取元素
    var top = $(".top");
    var main = $(".main");
    // console.log(main[0].offsetTop);
    var mianTop = main[0].offsetTop;
    // console.log(top);
    // (2) 页面滚动事件
    $(document).scroll(function () {
        // console.log(window.pageYOffset);
        // (3) 当页面的头部被卷去的部分大雨207时将侧边栏改为固定定位
        if (window.pageYOffset >= mianTop) {
            top.css("position", "fixed")
            top.css("right", 160 + 'px')
        } else {
            top.css("position", "absolute")
            top.css("right", 0 + 'px')
        }
    })



});